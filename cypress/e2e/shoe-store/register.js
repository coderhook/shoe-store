import {buildUser} from '../../support/generate'

describe('Registration', () => {
  it.skip('should register a new user', () => {
    const user = buildUser()

    cy.visit('/')
      .get('[href="/signin"]')
      .click()
      .get('#root > :nth-child(1) > :nth-child(1)')
      .findByText('Register')
      .click()
      .get('[data-testid=register-form]')
      .findByLabelText('Name')
      .type(user.name)
      .get('[data-testid=email]')
      .type(user.email)
      .get('[data-testid=password]')
      .type(user.password)
      .get('[data-testid=submit-register]')
      .click()

    cy.url()
      .should('eq', `${Cypress.config().baseUrl}/signin`)
      .window()
      .its('localStorage.token')
      .should('be.a', 'string')
  })

  it.skip('should show a error message when registration fails', () => {
    // currently is  not intercepting it because we aredoing it with fetch and cy only intercepts XMLHttpRequest,
    // I leave it here for when  changing to axios.
    cy.server().route({
      method: 'POST',
      url: 'http://localhost:3001/api/v1/auth/register',
      status: 500,
      response: {},
    })

    cy.visit('/signin')
      .get('#root > :nth-child(1) > :nth-child(1)')
      .findByText('Register')
      .click()
      .get('[data-testid=submit-register]')
      .click()
      .get('.form-content > p')
      .should('have.text', 'Registration failed!')
  })
})
