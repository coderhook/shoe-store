import {buildUser} from '../../support/generate'

describe('Login', () => {
  it.skip('should login a registered user', () => {
    cy.createUser().then(user => {
      // Test that loginworks properly
      cy.visit('/signin')
        .get('body')
        .get('[data-testid=email]')
        .type(user.email)
        .get('[data-testid=password]')
        .type(user.password)
        .get('[data-testid=submit-login]')
        .click()
        .get('.form-content > p')
        .should('have.text', 'Login Succesful.')
    })
  })
})
