describe('shoe-store', () => {
  it('can go throw the menu', () => {
    cy.visit('/')
      .get('[role="products-link"]')
      .click()
      .get('.primary-nav > [href="/styles"]')
      .click()
      .get('.secondary-nav > .cart')
      .click()

    cy.get('h1').should('have.text', 'Shopping Cart')
  })
})
