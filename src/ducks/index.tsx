import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form'
import {routerReducer} from 'react-router-redux'

import user from './user'
import products from './products'
import product from './product'
import collections from './collections'
import cart from './cart'
import categories from './categories'
import checkout from './checkout'
import styles from './styles'
import payments from './payments'

export type RootReducer = ReturnType<typeof rootReducer>

export type State = RootReducer

const rootReducer = combineReducers({
  user,
  product,
  products,
  collections,
  cart,
  categories,
  checkout,
  styles,
  payments,
  router: routerReducer,
  form: formReducer,
})

export default rootReducer
