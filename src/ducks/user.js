import {GetUserLogin, GetUserRegistration} from '../api'
import {signIn, signUp} from '../components/User/amplifyAPI'

export const SET_USER = 'user/SET_USER'

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      return {...state, user: action.payload}

    default:
      return {...state}
  }
}

export const setUser = user => ({
  type: SET_USER,
  payload: user,
})

export const loginUser = async (values, dispatch, router) => {
  try {
    // From own API
    // const result = await GetUserLogin(values)
    // const body = await result.json()

    //FRom Amplify
    const body = await signIn(values.email, values.password)
    console.log('INSIDE loginUser', {body})
    if (body?.data?.token) {
      localStorage.setItem('token', body.data.token)
      dispatch(setUser({...body.data.user}))
      // router.push('/')

      return {
        status: 'success',
        message: body.message,
      }
    }

    return {
      status: body.status,
      message: body.message,
    }
  } catch (e) {
    return e
  }
}

export const registerUser = async (values, dispatch, router) => {
  try {
    // const result = await GetUserRegistration(values)
    // const body = await result.json()

    const {email, password, name} = values
    const body = await signUp(email, password, name)

    if (body?.data?.token) {
      localStorage.setItem('token', body.data.token)

      dispatch(setUser({...body.data.user}))
      router.push('/')
      return {
        status: 'success',
        message: body.message,
      }
    }
    return {body, message: body.message}
  } catch (e) {
    return e
  }
}
