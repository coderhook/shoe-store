import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import {BrowserRouter} from 'react-router-dom'
import store, {history} from './store'
import App from './components/App'
import HeaderNav from './components/global/HeaderNav'
import Amplify from 'aws-amplify'
import config from './aws-exports'

import './index.css'

Amplify.configure(config)
const target = document.getElementById('root')

render(
  <Provider store={store}>
    <BrowserRouter history={history}>
      <div>
        {/* <HeaderNav /> */}
        <App />
      </div>
    </BrowserRouter>
  </Provider>,
  target,
)
