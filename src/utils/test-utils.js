// test-utils.js
import React from 'react'
import {render as rtlRender} from '@testing-library/react'
import {Provider} from 'react-redux'
import {Router} from 'react-router-dom'
// import store from '../store'
import {createMemoryHistory} from 'history'
import {createStore} from 'redux'
import rootReducer from '../ducks/index'

function customRender(
  ui,
  {
    initialState,
    store = createStore(rootReducer, initialState),
    route = '/',
    history = createMemoryHistory({initialEntries: [route]}),
    ...renderOptions
  } = {},
) {
  function Wrapper({children}) {
    return (
      <Provider store={store}>
        <Router history={history}>{children}</Router>
      </Provider>
    )
  }
  return {
    ...rtlRender(ui, {
      wrapper: Wrapper,
      ...renderOptions,
    }),
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests
    history,
    store,
  }
}

// re-export everything
export * from '@testing-library/react'

// override render method
export {customRender as render}
