export type FixMeLater = any

export type Optional<T> = T | null | undefined
