import '@testing-library/jest-dom/extend-expect'
import 'jest-axe/extend-expect'
import React from 'react'
import user from '@testing-library/user-event'
import {render} from '../../utils/test-utils'
import CheckoutForm from './CheckoutForm'
import {axe} from 'jest-axe'

jest.spyOn(console, 'warn').mockImplementation(() => {})

describe('CHECKOUT', () => {
  test('should render the form', () => {
    const res = render(<CheckoutForm />)

    expect(res.getByTestId('checkout-form')).toBeInTheDocument()
    expect(res.getByTestId('checkout-summary')).toBeInTheDocument()
  })

  test('the form is accesible', async () => {
    const res = render(<CheckoutForm />)
    const results = await axe(res.container)

    expect(results).toHaveNoViolations()
  })

  describe('Your Details', () => {
    test('should return values introduced in the form', () => {
      const res = render(<CheckoutForm />)
      const name = res.getByLabelText('Name')
      const email = res.getByLabelText('Email')
      const password = res.getByLabelText('Password')

      user.type(name, 'Test')
      user.type(email, 'test@test.com')
      user.type(password, 'testpassword')

      expect(name.value).toBe('Test')
      expect(email.value).toBe('test@test.com')
      expect(password.value).toBe('testpassword')
    })
  })
})
