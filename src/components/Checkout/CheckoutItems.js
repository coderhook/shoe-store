import React from 'react'
import {useSelector} from 'react-redux'
import ProductImage from '../Products/ProductImage'

const CheckoutItems = () => {
  const {items, products} = useSelector(state => ({
    items: state.cart.cart,
    products: state.products.products,
  }))

  return (
    <div>
      {items.map(item => {
        const product = products.data.find(function (product) {
          return product.model === item.model
        })

        const background = product.background_colour

        return (
          <div className="checkout-product" key={item.id}>
            <div className="product-image" aria-hidden="true">
              <ProductImage
                products={products}
                product={product}
                background={background}
              />
            </div>
            <div className="product-info">
              <div className="product-title">
                {product.name + ' X ' + item.quantity}
              </div>
              <div className="price">
                <span className="hide-content">Product subtotal: </span>
                {'$' + item.price / 100}
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default CheckoutItems
