import '@testing-library/jest-dom/extend-expect'
import 'jest-axe/extend-expect'
import React from 'react'
import {render} from '../utils/test-utils'
import {fireEvent} from '@testing-library/react'

import App from './App'

jest.spyOn(console, 'warn').mockImplementation(() => {})

describe('Routes', () => {
  test('should allow to navigate between Products and Home', () => {
    const initialState = {
      products: {fetched: true, products: {data: []}},
      categories: {fetched: true},
      collections: {fetched: true},
    }
    const res = render(<App />, {initialState})

    const productsLink = res.getByRole('products-link')

    fireEvent.click(productsLink)

    expect(res.getByRole('products-header')).toBeInTheDocument()

    const homeLink = res.getByRole('logo-link')

    expect(homeLink).toBeInTheDocument()
    expect(res.queryByRole('home-header')).not.toBeInTheDocument()

    fireEvent.click(homeLink)

    expect(res.getByRole('home-header')).toBeInTheDocument()
    expect(res.queryByRole('products-header')).not.toBeInTheDocument()
  })

  test('should allow to navigate to cart', () => {
    const initialState = {
      products: {fetched: true, products: {data: []}},
      categories: {fetched: true},
      collections: {fetched: true},
      cart: {fetched: true, cart: [{}, {}]},
    }
    const res = render(<App />, {initialState})

    const cartLink = res.getByTestId('cart-link')

    expect(cartLink).toBeInTheDocument()

    fireEvent.click(cartLink)

    expect(res.getByRole('main')).toBeInTheDocument()
  })

  test('should land on page NotFound when on a not register route', () => {
    const res = render(<App />, {route: '/wrong-route'})

    expect(res.getByRole('not-found-page')).toBeInTheDocument()
  })
})
