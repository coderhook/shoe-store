import '@testing-library/jest-dom/extend-expect'
import 'jest-axe/extend-expect'
import 'mutationobserver-shim'

import React from 'react'
import {waitFor} from '@testing-library/react'
import user from '@testing-library/user-event'
import {cleanup, render} from '../../../utils/test-utils'
import CheckoutForm from '../CheckoutForm'
import {axe} from 'jest-axe'
import * as L from '../../User/LoginUser'
import * as mockStripe from '../createSessionAndRedirect'

jest.spyOn(console, 'warn').mockImplementation(() => {})

jest.mock('react-hook-form', () => ({
  useForm: () => ({
    register: jest.fn(),
    handleSubmit: jest.fn(),
    errors: jest.fn(),
  }),
}))

jest.mock('../createSessionAndRedirect')

const LoginUserMock = jest.spyOn(L, 'default')

beforeEach(() => {
  cleanup()
  LoginUserMock.mockClear()
})

describe('CHECKOUT', () => {
  test('should render the form', () => {
    const res = render(<CheckoutForm />)
    expect(res.getByTestId('checkout-form')).toBeInTheDocument()
    expect(res.getByTestId('checkout-summary')).toBeInTheDocument()
  })

  test('the form is accesible', async () => {
    const res = render(<CheckoutForm />)

    const checkoutForm = res.getByTestId('checkout-form')
    const results = await axe(checkoutForm)

    expect(results).toHaveNoViolations()
  })

  describe('Your Details', () => {
    test('should show login/register option when there is no user logged in', () => {
      const res = render(<CheckoutForm />)

      expect(res.queryByTestId('welcome')).not.toBeInTheDocument()
      expect(LoginUserMock).toHaveBeenCalled()
    })

    test('should return user name if user is logged in', () => {
      const initialState = {user: {name: 'testUser'}}
      const res = render(<CheckoutForm />, {initialState})

      expect(res.getByTestId('welcome')).toHaveTextContent('Welcome, testUser!')
    })
  })

  describe('Shipping', () => {
    test('should show Shipping form fields', () => {
      const initialState = {user: {name: 'testUser'}}

      const res = render(<CheckoutForm />, {initialState})
      expect(res.getByTestId('shipping-form')).toBeInTheDocument()
    })

    test('should be able to fill the shipping form', () => {
      const initialState = {user: {name: 'testUser'}}

      const res = render(<CheckoutForm />, {initialState})
      const firstName = res.getAllByLabelText(/first name/i)[0]
      const lastName = res.getAllByLabelText(/last name/i)[0]
      const company = res.getAllByLabelText(/company/i)[0]
      const addressLine1 = res.getAllByLabelText(/address line 1/i)[0]

      const shippingFieldSet = res.getByTestId('checkout-form')

      user.type(firstName, 'first test name')
      user.type(lastName, 'last test name')
      user.type(company, 'company test name')
      user.type(addressLine1, 'addressLine1 test')

      // expect(shippingFieldSet).toHaveFormValues({
      //   firstName: 'first test name',
      // })

      expect(firstName).toHaveValue('first test name')
    })
  })

  describe('Billing', () => {
    test('should show Billing form fields', () => {
      const initialState = {user: {name: 'testUser'}}

      const res = render(<CheckoutForm />, {initialState})
      expect(res.getByTestId('billing-form')).toBeInTheDocument()
    })
  })

  describe('Payment', () => {
    test('should show Payment section of the form', () => {
      const res = render(<CheckoutForm />)
      expect(res.getByText(/payment details/i)).toBeInTheDocument()
    })

    test('should call create stripe session on submittin the form', async () => {
      const customerId = '212'
      mockStripe.createCustomerAccount.mockResolvedValueOnce({customerId})
      mockStripe.createSessionAndRedirect.mockResolvedValueOnce({
        data: 'payment done',
      })

      const res = render(<CheckoutForm />)
      const paymentForm = res.getByTestId('payment-form')

      user.type(res.getByLabelText(/name on card/i), 'Test name card')
      user.type(res.getByLabelText(/card number/i), '1234 1234 1234 1234')
      user.selectOptions(res.getByLabelText(/card expiry month/i), '08')
      user.selectOptions(res.getByLabelText(/card expiry year/i), '2020')
      user.type(res.getByLabelText(/cvc code/i), '321')

      user.click(res.getByTestId('submit-form'))

      const cart = []
      const CheckoutTemplate = {}

      await waitFor(() => {
        expect(mockStripe.createCustomerAccount).toHaveBeenCalledTimes(1)
      })

      expect(mockStripe.createSessionAndRedirect).toHaveBeenCalledTimes(1)
      expect(mockStripe.createSessionAndRedirect).toHaveBeenCalledWith(
        cart,
        expect.anything(),
        customerId,
      )
    })
  })
})
