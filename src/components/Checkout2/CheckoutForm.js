import React from 'react'
import CheckoutSummary from './CheckoutSummary'
import {reduxForm} from 'redux-form'
import {useSelector} from 'react-redux'
import {
  createSessionAndRedirect,
  createCustomerAccount,
} from './createSessionAndRedirect'

import {SUBMIT_PAYMENT, PAYMENT_COMPLETE} from '../../ducks/payments'
import {State} from '../../ducks'
import {FixMeLater} from '../../utils/interfaces'
import CountryList from './components/CountryList'
import DetailsSection from './components/DetailsSection'
import BillingSection from './components/BillingSection'
import ShippingSection from './components/ShippingSection'
import PaymentSection from './components/PaymentSection'
import SigninContainer from '../User/SigninContainer'

var CheckoutTemplate = {
  customer: {
    name: 'John Doe',
    email: 'john@doe.co',
  },
  shipping_address: {
    first_name: 'John',
    last_name: 'Doe',
    line_1: '2nd Floor British India House',
    line_2: '15 Carliol Square',
    city: 'Newcastle Upon Tyne',
    postcode: 'NE1 6UF',
    county: 'Tyne & Wear',
    country: 'United Kingdom',
  },
  billing_address: {
    first_name: 'John',
    last_name: 'Doe',
    line_1: '2nd Floor British India House',
    line_2: '15 Carliol Square',
    city: 'Newcastle Upon Tyne',
    postcode: 'NE1 6UF',
    county: 'Tyne & Wear',
    country: 'United Kingdom',
  },
}
var PaymentTemplate = {
  gateway: 'stripe',
  method: 'purchase',
  first_name: 'John',
  last_name: 'Doe',
  number: '4242424242424242',
  month: '08',
  year: '2020',
  verification_value: '123',
}
const required = value => (value ? undefined : 'Required')
const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined

const CheckoutForm = props => {
  // console.log({props})
  const {cart} = useSelector(state => state.cart)
  const handleKeyDown = function (e) {
    if (e.key === 'Enter' && e.shiftKey === false) {
      e.preventDefault()
    }
  }

  const user = useSelector(state => state.user)
  const mySubmit = async values => {
    CheckoutTemplate.customer.name = values.name
    CheckoutTemplate.customer.email = values.email

    CheckoutTemplate.billing_address.first_name = values.billing_firstname
    CheckoutTemplate.billing_address.last_name = values.billing_lastname
    CheckoutTemplate.billing_address.line_1 = values.billing_address_1
    CheckoutTemplate.billing_address.line_2 = values.billing_address_2
    CheckoutTemplate.billing_address.city = values.billing_state
    CheckoutTemplate.billing_address.county = values.billing_postcode
    CheckoutTemplate.billing_address.country = values.billing_country

    CheckoutTemplate.shipping_address.first_name = values.shipping_firstname
    CheckoutTemplate.shipping_address.last_name = values.shipping_lastname
    CheckoutTemplate.shipping_address.line_1 = values.shipping_address_1
    CheckoutTemplate.shipping_address.line_2 = values.shipping_address_2
    CheckoutTemplate.shipping_address.city = values.shipping_state
    CheckoutTemplate.shipping_address.county = values.shipping_postcode
    CheckoutTemplate.shipping_address.country = values.shipping_country

    //Creates customer account in stripe
    const {customerId} = await createCustomerAccount(CheckoutTemplate)
    //Create payment session and redirects to payment stripe page
    const result = await createSessionAndRedirect(
      cart,
      CheckoutTemplate,
      customerId,
    )
    return result
  }

  return (
    <main role="main" id="container" className="main-container push">
      <section className="checkout">
        <div className="content">
          <CheckoutSummary />
          <div className="checkout-form">
            <div className="form-header" style={{marginBottom: '-5rem'}}>
              <h2>Your details</h2>
            </div>
            {/* <DetailsSection /> */}
            {!user.name ? (
              <SigninContainer />
            ) : (
              <div style={{margin: '8rem 3rem 0 3rem'}}>
                <p data-testid="welcome">Welcome, {user.name}!</p>
                <p>
                  Lets continue with the shipping address on we can send you the
                  shoes.
                </p>
              </div>
            )}
            <form
              name="checkout-form"
              data-testid="checkout-form"
              noValidate
              onSubmit={props.handleSubmit(mySubmit)}
              onKeyDown={e => {
                handleKeyDown(e)
              }}
              style={{maxWidth: '100%', padding: 0}}
            >
              {user.name && (
                <fieldset>
                  <button type="button" className="continue">
                    Continue
                  </button>
                </fieldset>
              )}

              <ShippingSection />
              <BillingSection />

              <fieldset
                className="payment collapsed"
                data-testid="payment-form"
              >
                <div className="form-header inactive">
                  <h2>Payment details</h2>
                </div>
                <div className="form-content">
                  <PaymentSection />
                  <button
                    type="submit"
                    className="pay"
                    aria-live="polite"
                    data-testid="submit-form"
                  >
                    <div className="loading-icon">
                      <span className="hide-content">Processing</span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 52.7 46.9"
                        aria-hidden="true"
                      >
                        <path
                          fill="currentColor"
                          d="M47.8,15.9c0,2.8-1,5.6-3.2,7.6L26.4,41.7L8.1,23.5c-4.3-4.3-4.3-11.1,0-15.4c2.1-2.1,4.9-3.2,7.7-3.2c2.8,0,5.6,1,7.6,3.2
                            l2.9,2.9l2.9-2.9c4.3-4.3,11.1-4.3,15.4,0C46.7,10.3,47.8,13.1,47.8,15.9z"
                        />
                      </svg>
                    </div>
                    <span className="copy">Pay</span>
                  </button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </section>
    </main>
  )
}

export default reduxForm({form: 'CheckoutForm'})(CheckoutForm)
