import {loadStripe} from '@stripe/stripe-js'
const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY)

const url = 'http://localhost:3001/api/v1/checkout'

export const createSessionAndRedirect = async (cart, data, customerId) => {
  // Call your backend to create the Checkout Session—see previous step
  const {sessionId} = await createCheckoutSession(cart, data, customerId)
  // When the customer clicks on the button, redirect them to Checkout.
  const stripe = await stripePromise

  return await stripe.redirectToCheckout({
    sessionId,
  })
  // If `redirectToCheckout` fails due to a browser or network
  // error, display the localized error message to your customer
  // using `error.message`.
}

const createCheckoutSession = async (cart, data, customerId) => {
  const config = {
    method: 'POST',
    body: JSON.stringify({cart, data, customerId}),
    headers: {
      'Content-Type': 'application/json',
    },
  }
  const response = await fetch(url, config)

  const {session_id} = await response.json()

  return {sessionId: session_id}
}

export const createCustomerAccount = async data => {
  const config = {
    method: 'POST',
    body: JSON.stringify({userData: data}),
    headers: {
      'Content-Type': 'application/json',
    },
  }
  const response = await fetch(url + '/create-customer', config)

  const {id} = await response.json()

  return {customerId: id}
}
