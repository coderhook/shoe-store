import React from 'react'
import {Field} from 'redux-form'

const PaymentSection = () => {
  return (
    <div className="form-fields">
      <label className="input-wrap name">
        <span className="hide-content">Name on card</span>
        <Field
          component="input"
          required="required"
          placeholder="Name on card"
          name="card_name"
          type="text"
          aria-label="Name on card"
        />
      </label>
      <label className="input-wrap card required">
        <span className="hide-content">Card number</span>
        <Field
          component="input"
          required="required"
          placeholder="Card number"
          name="card-number"
          maxLength="23"
          type="tel"
          aria-label="Card number"
        />
      </label>
      <div className="input-wrap expiry-month">
        <label className="select-fallback required">
          <span className="hide-content">Card expiry month</span>
          <select id="expiry-month" required="required" name="expiry-month">
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
          </select>
        </label>
      </div>
      <div className="input-wrap expiry-year">
        <label className="select-fallback required">
          <span className="hide-content">Card expiry year</span>
          <select id="expiry-year" required="required" name="expiry-year">
            <option value="2017">2017</option>
            <option value="2018">2018</option>
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
            <option value="2022">2022</option>
            <option value="2023">2023</option>
            <option value="2024">2024</option>
            <option value="2025">2025</option>
            <option value="2026">2026</option>
            <option value="2027">2027</option>
          </select>
        </label>
      </div>
      <label className="input-wrap cvc required">
        <span className="hide-content">CVC code</span>
        <Field
          component="input"
          required="required"
          placeholder="CVC"
          maxLength="4"
          name="card_cvc"
          type="tel"
          aria-label="CVC"
        />
      </label>
    </div>
  )
}

export default PaymentSection
