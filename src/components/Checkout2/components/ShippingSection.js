import React from 'react'
import {Field} from 'redux-form'
import CountryList from './CountryList'

const ShippingSection = () => {
  return (
    <fieldset
      className="shipping collapsed"
      data-testid="shipping-form"
      form="checkout-form"
    >
      <div className="form-header inactive">
        <h2>Shipping address</h2>
      </div>
      <div className="form-content">
        <div className="form-fields">
          <label className="input-wrap firstname required">
            <span className="hide-content">First name</span>
            <Field
              component="input"
              required="required"
              placeholder="First Name"
              name="shipping_firstname"
              type="text"
              aria-label="First name"
            />
          </label>
          <label className="input-wrap lastname required">
            <span className="hide-content">Last name</span>
            <Field
              component="input"
              required="required"
              placeholder="Last Name"
              name="shipping_lastname"
              type="text"
              aria-label="Last name"
            />
          </label>
          <label className="input-wrap company">
            <span className="hide-content">Company</span>
            <Field
              component="input"
              placeholder="Company"
              name="shipping_company"
              type="text"
              aria-label="Company"
            />
          </label>
          <label className="input-wrap address-1 required">
            <span className="hide-content">Address line 1</span>
            <Field
              component="input"
              required="required"
              placeholder="Address Line 1"
              name="shipping_address_1"
              type="text"
              aria-label="Address line 1"
            />
          </label>
          <label className="input-wrap address-2">
            <span className="hide-content">Address line 2</span>
            <Field
              component="input"
              placeholder="Address Line 2"
              name="shipping_address_2"
              type="text"
              aria-label="Address line 2"
            />
          </label>
          <label className="input-wrap state required">
            <span className="hide-content">State or county</span>
            <Field
              component="input"
              required="required"
              placeholder="State / County"
              name="shipping_state"
              type="text"
              aria-label="State / County"
            />
          </label>
          <label className="input-wrap postcode required">
            <span className="hide-content">Postcode</span>
            <Field
              component="input"
              required="required"
              placeholder="Postcode"
              name="shipping_postcode"
              type="text"
              aria-label="Postcode"
            />
          </label>
          <CountryList />
        </div>
        <button type="button" className="continue">
          Continue
        </button>
      </div>
    </fieldset>
  )
}

export default ShippingSection
