import React from 'react'
import {Field} from 'redux-form'
import {FixMeLater} from '../../../utils/interfaces'

const required = (value: any) => (value ? undefined : 'Required')

const renderField = ({
  input,
  label,
  type,
  meta: {touched, error, warning},
}: FixMeLater) => (
  <div>
    <label>{label}</label>
    <div>
      <input
        component="input"
        aria-label={label}
        {...input}
        placeholder={label}
        type={type}
      />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
)

const DetailsSection = () => {
  return (
    <fieldset className="details">
      <div className="form-header">
        <h2>Your details</h2>
      </div>
      <div className="form-content"></div>
      <div className="form-fields">
        <label className="input-wrap name required">
          <span className="hide-content">Name</span>
          <Field
            component={renderField}
            className="name"
            validate={[required]}
            placeholder="Name"
            name="name"
            type="text"
            aria-label="Name"
          />
        </label>
        <label className="input-wrap email required">
          <span className="hide-content">Email address</span>
          <Field
            component="input"
            className="email"
            validate={[required]}
            placeholder="Email address"
            name="email"
            type="email"
            aria-label="Email"
          />
        </label>
        <label className="input-wrap password required">
          <span className="hide-content">Password</span>
          <Field
            component="input"
            className="password"
            // required="required"
            placeholder="Password"
            name="password"
            type="password"
            aria-label="Password"
          />
        </label>
      </div>
      <button
        type="button"
        className="continue"
        // onClick={props.handleSubmit(loginOrRegister)}
      >
        Continue
      </button>
    </fieldset>
  )
}

export default DetailsSection
