import React from 'react'
import {Switch, Route} from 'react-router-dom'

import Home from './Home/Home'
import Cart from './Cart/Cart'
import CheckoutContainer from './Checkout2/CheckoutContainer'
import StylesContainer from './Styles/StylesContainer'
import ProductsContainer from './Products/ProductsContainer'
import SingleProductContainer from './Products/SingleProductContainer'
import OneClickCheckout from './Checkout2/OneClickCheckout'
import OrderConfirmationContainer from './Orders/OrderConfirmationContainer'
import NotFound from './global/NotFound'
import SuccessPayment from './global/SuccessPayment'
// import MobileNav from './global/Mobile/MobileNav';
import Footer from './global/Footer'
import SigninContainer from './User/SigninContainer'

const App: React.FC = () => (
  <div>
    {/* <MobileNav /> */}

    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/signin" component={SigninContainer} />
      <Route path="/cart" component={Cart} />
      <Route path="/styles" component={StylesContainer} />
      <Route path="/products" component={ProductsContainer} />
      <Route path="/checkout" component={CheckoutContainer} />
      <Route
        path="/order-confirmation"
        component={OrderConfirmationContainer}
      />
      <Route path="/product/:id" component={SingleProductContainer} />
      <Route
        path="/one-click-checkout/:productId"
        component={OneClickCheckout}
      />
      <Route path="/success" component={SuccessPayment} />
      <Route path="*" component={NotFound} />
    </Switch>

    <Footer />
  </div>
)

export default App
