import React from 'react'
import {useDispatch, useSelector} from 'react-redux'

import SingleProduct from './SingleProduct'
import CartHeader from '../Cart/CartHeader'
import ProductHeader from './ProductHeader'
import Loading from '../global/Loading'
import MobileNav from '../global/Mobile/MobileNav'
import { State } from '../../ducks'
import { GetProducts } from '../../ducks/products'
import { FixMeLater } from '../../utils/interfaces'

const Product: React.FC = () => {
  const products: FixMeLater = useSelector((state: State) => state.products)
  const dispatch = useDispatch()

  React.useEffect(() => {
    if (!products.fetched) {
      dispatch(GetProducts())
    }  }, [])

  if (products) {
    return (
      <div>
        <MobileNav />
        <ProductHeader />
        <SingleProduct />
      </div>
    )
  } else {
    return (
      <div>
        <MobileNav />
        <CartHeader />
        <Loading />
      </div>
    )
  }
}

export default Product
