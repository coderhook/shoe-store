import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import {render} from '../../../utils/test-utils'
import {useSelector} from 'react-redux'
import productsMock from '../../../utils/sampleData/products.json'
import SingleProduct from '../SingleProduct'

jest.spyOn(console, 'warn').mockImplementation(() => {})

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}))

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useParams: jest.fn().mockReturnValue({id: '1'}),
}))

describe('SingleProduct', () => {
  useSelector.mockReturnValue(productsMock.data)

  test('should render SingleProduct component', () => {
    const res = render(<SingleProduct />)

    const singleProduct = res.getByRole('single-product')

    expect(singleProduct).toBeInTheDocument()
  })

  test('should  render as sizes that have product available', () => {
    const res = render(<SingleProduct />)

    const sizesAvailable = res.getAllByTestId('size')
    const productSizesAvailable = Object.values(
      productsMock.data[0].inventory,
    ).filter(s => s > 0)

    expect(sizesAvailable.length).toBe(productSizesAvailable.length)
  })
})
