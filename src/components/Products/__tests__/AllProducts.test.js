import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import {render} from '../../../utils/test-utils'
import AllProducts from '../AllProducts'
import {useSelector} from 'react-redux'
import productsMock from '../../../utils/sampleData/products.json'

jest.spyOn(console, 'warn').mockImplementation(() => {})

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}))

describe('AllProducts', () => {
  test('should render AllProducts component', () => {
    // jest.spyOn('A.useAllProducts', () => jest.fn().mockReturnValue([]))
    useSelector.mockReturnValue([])
    const res = render(<AllProducts />)
    const main = res.getByRole('main')

    expect(main).toBeInTheDocument()
  })

  test("should show 'You Do not have any product' when there are no products", () => {
    useSelector.mockReturnValue([])

    const res = render(<AllProducts />)
    const main = res.getByRole('main')

    expect(main.textContent).toBe('There are no products available')
  })

  test('should show products loaded on the store', () => {
    useSelector.mockReturnValue(productsMock.data)

    const res = render(<AllProducts />)
    const main = res.getByRole('main')

    expect(res.getByText(/product 1/i)).toBeInTheDocument()
    expect(res.getByText(/product 2/i)).toBeInTheDocument()
  })

  // test.only('hook', () => {
  //   jest.spyOn(A, 'useAllProducts')
  //   A.useAllProducts.mockImplementation(() => 'productsMock.data')

  //   const res = render(<AllProducts />)
  //   const main = res.getByRole('main')

  //   expect(res.getByText(/product 1/i)).toBeInTheDocument()
  //   expect(res.getByText(/product 2/i)).toBeInTheDocument()
  // })
})
