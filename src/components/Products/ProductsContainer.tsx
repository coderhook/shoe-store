import React, {useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'

import AllProducts from './AllProducts'
import ProductsHeader from './ProductsHeader'
import MobileNav from '../global/Mobile/MobileNav'
import Loading from '../global/Loading'

import {GetProducts} from '../../ducks/products'
import NoProductsAvailable from '../global/NoProductsAvailable'
import { State } from '../../ducks'

const ProductsContainer: React.FC = () => {
  const products = useSelector((state: State) => state.products)
  const dispatch = useDispatch()

  useEffect(() => {
    if (!products.fetched) {
      dispatch(GetProducts())
    }
  }, [])

  return (
    <div>
      <MobileNav />
      <ProductsHeader />
      {!products.fetched ? (
        <Loading />
      ) : products.products.data.length > 0 ? (
        <AllProducts />
      ) : (
        <NoProductsAvailable />
      )}
    </div>
  )
}

export default ProductsContainer
