import React from 'react'
import { IProduct } from '../interfaces'

interface Props {
  product: IProduct | undefined
}

const ProductImage:React.FC<Props> = ({product}): JSX.Element => {

  const placeholder = 'https://via.placeholder.com/150'

  const image = product?.images[0] ? product.images[0] : placeholder
  const background = product?.background_colour || '#ffffff'

  return (
    <img
      alt={product?.name + '-' + product?.description || 'placeHolder'}
      src={image}
      style={{background}}
  />
  )

}

export default ProductImage
