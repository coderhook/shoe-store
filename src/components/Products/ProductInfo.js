import React from 'react'

const ProductInfo = product => {
  return (
    <div className="product-info">
      <div className="product-details">
        <div className="header">
          <h3>Product details</h3>
        </div>
        <div className="details-body">
          <div className="row">
            <div className="label">Weight</div>
            <div className="value">1.5kg</div>
          </div>
          <div className="row">
            <div className="label">Finish</div>
            <div className="value">{product.finish}</div>
          </div>
          <div className="row">
            <div className="label">Material</div>
            <div className="value">{product.material}</div>
          </div>
          <div className="row">
            <div className="label">Bulb type</div>
            <div className="value">{product.bulb}</div>
          </div>
          <div className="row">
            <div className="label">Max Watt</div>
            <div className="value">{product.max_watt}</div>
          </div>
          <div className="row">
            <div className="label">Bulb Qty</div>
            <div className="value">{product.bulb_qty}</div>
          </div>
          <div className="row">
            <div className="label">SKU</div>
            <div className="value sku">{product.sku}</div>
          </div>
        </div>
      </div>
      <div className="product-details">
        <div className="header">
          <h3>Dimensions (cm)</h3>
        </div>
        <div className="details-body">
          <div className="row">
            <div className="label">Height</div>
            <div className="value">156</div>
          </div>
          <div className="row">
            <div className="label">Width</div>
            <div className="value">80</div>
          </div>
          <div className="row">
            <div className="label">Depth</div>
            <div className="value">80</div>
          </div>
        </div>
      </div>
      <div className="product-details">
        <div className="header">
          <h3>Delivery & returns</h3>
        </div>
        <div className="details-body">
          <div className="row">
            <div className="label">Dispatch</div>
            <div className="value">Within 2 weeks</div>
          </div>
          <div className="row">
            <div className="label">Delivery</div>
            <div className="value">$5.95</div>
          </div>
        </div>
        <div className="footer">
          <p>
            Read the <a href="/">delivery and returns policy</a>.
          </p>
        </div>
      </div>
    </div>
  )
}

export default ProductInfo
