import React, {Component} from 'react'
import {connect} from 'react-redux'
import HeaderNav from '../global/HeaderNav'

class ProductHeader extends Component {
  componentWillMount() {
    const script = document.createElement('script')

    script.src = '../../js/production.min.js'
    script.async = false

    document.body.appendChild(script)
  }

  render() {
    return (
      <header className="push">
        <HeaderNav />
      </header>
    )
  }
}

const mapStateToProps = state => state

export default connect(mapStateToProps)(ProductHeader)
