import React from 'react'
import {useSelector} from 'react-redux'
import ProductImage from './ProductImage'
import {useHistory} from 'react-router-dom'
import NoProductsAvailable from '../global/NoProductsAvailable'
import { IProduct } from '../interfaces'
import { State } from '../../ducks'

export const isThereACurrencyPrice = (product: IProduct): JSX.Element => {
  try {
    return <div className="price">{product.price.unit_amount / 100} €</div>
  } catch (e) {
    return <div className="price">Price not available</div>
  }
}

export const useAllProducts = () => {
  const products = useSelector((state: State): IProduct[] => state.products.products.data)

  return {
    products,
  }
}

const AllProducts: React.FC = () => {
  const {products} = useAllProducts()
  const history = useHistory()

  if (!products) return null

  if (products.length > 0) {
    return (
      <main role="main" id="container" className="main-container push">
        <section className="products">
          <div className="content">
            <div className="product-list" role="products-list">
              {products.map(function (product) {
                const background = product.background_colour
                  ? product.background_colour
                  : '#d9d9d9'

                return (
                  <a
                    className="product-item"
                    href={'product/' + product.model}
                    key={product.model}
                    onClick={e => {
                      e.preventDefault()
                      history.push('/product/' + product.model)
                    }}
                  >
                    <div
                      className="product-image"
                      style={{background: background}}
                    >
                      <ProductImage product={product} />
                    </div>
                    <div className="overlay">
                      <div
                        className="overlay-background"
                        style={{background: '#aaaaaa'}}
                      />
                      <div className="overlay-content">
                        <div className="title">{product.name}</div>
                        {isThereACurrencyPrice(product)}
                      </div>
                    </div>
                  </a>
                )
              })}
            </div>
          </div>
        </section>
      </main>
    )
  }

  return <NoProductsAvailable />
}

export default AllProducts
