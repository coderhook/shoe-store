import React, {useState} from 'react'

interface Props {
  quantity: number
  quantitySelected: (q: number) => void
  sizeQuantity: number
}

const Quantity: React.FC<Props> = ({quantity, quantitySelected, sizeQuantity}) => {
  const [_quantity, _setQuantity] = useState<number>(quantity)

  return (
    <div className="quantity-input">
      <p className="hide-content">Product quantity.</p>
      <p className="hide-content">
        Change the quantity by using the buttons, or alter the input directly.
      </p>
      <button
        type="button"
        className="decrement number-button"
        disabled={_quantity < 1}
        onClick={() => {
          _setQuantity(_quantity - 1)
          quantitySelected(_quantity - 1)
        }}
      >
        <span className="hide-content">Decrement quantity</span>
        <span aria-hidden="true">-</span>
      </button>

      <input
        className="quantity"
        name="number"
        type="number"
        min="1"
        max="2"
        value={quantity}
        size={2}
        disabled
        onChange={event => {
          _setQuantity(+event.target.value)
          quantitySelected(+event.target.value)
        }}
      />
      <button
        type="button"
        className="increment number-button"
        disabled={_quantity >= sizeQuantity}
        onClick={() => {
          _setQuantity(_quantity + 1)
          quantitySelected(_quantity + 1)
        }}
      >
        <span className="hide-content">Increment quantity</span>
        <span aria-hidden="true">+</span>
      </button>
    </div>
  )
}

export default Quantity
