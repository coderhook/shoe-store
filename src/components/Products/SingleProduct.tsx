import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {useParams} from 'react-router'
import ProductImage from './ProductImage'
import Sizes from './Sizes'
import Quantity from './Quantity'
import {isThereACurrencyPrice} from './AllProducts'

import {UPDATE_QUANTITY} from '../../ducks/product'
import {FETCH_CART_START, FETCH_CART_END, CART_UPDATED} from '../../ducks/cart'
import { IProduct, StateProducts } from '../interfaces'
import ProductInfo from './ProductInfo'

const SingleProduct: React.FC = () => {
  const products = useSelector((state: StateProducts): IProduct[] | undefined => state.products?.products?.data)
  const [quantity, setQuantity] = useState<number>(1)
  const [selectedSize, setSelectedSize] = useState<string | null>(null)
  const dispatch = useDispatch()
  const {id}: {id: string} = useParams()

  const product = products?.find(product => product.model === id)

  const sizeSelected = (size: string | null) => {
    setSelectedSize(size)
  }

  const updateQuantity = (quantity: number) => {
    dispatch((dispatch: (arg0: { type: string; payload: number }) => void) => {
      dispatch({type: UPDATE_QUANTITY, payload: quantity})
    })
  }

  const addToCart = (model: string, size: string | null, quantity: number, price: number) => {
    const item = {model, size, quantity, price}

    dispatch({type: CART_UPDATED, payload: item, gotNew: false})

    setQuantity(1)
    setSelectedSize(null)
    // this.props.dispatch(dispatch => {
    //   api
    //     .AddCart(id, product.quantity)

    //     .then(cart => {
    //       console.log(cart);
    //       dispatch({ type: CART_UPDATED, gotNew: false });
    //     })

    //     .then(() => {
    //       dispatch({ type: FETCH_CART_START, gotNew: false });

    //       api
    //         .GetCartItems()

    //         .then(cart => {
    //           dispatch({ type: FETCH_CART_END, payload: cart, gotNew: true });
    //         });
    //     })
    //     .catch(e => {
    //       console.log(e);
    //     });
    // });
  }

  if (!product) return null

  const background = product.background_colour

  return (
    <main role="single-product" id="container" className="main-container push">
      <section className="product">
        <div className="content">
          <div className="product-listing">
            <div className="product-image">
              <ProductImage product={product} />
            </div>
            <div className="product-description">
              <h2>{product.name}</h2>
              <p className="manufacturer">
                <span className="hide-content">Manufactured </span>By{' '}
                <span className="word-mark">
                  I<span className="love">Love</span>Shoes
                </span>
              </p>
              {isThereACurrencyPrice(product)}
              <div className="description">
                <p className="hide-content">Product details:</p>
                <p>{product.description}</p>
              </div>
              <form className="product" noValidate>
                <p>Sizes available: </p>

                <Sizes
                  product={product}
                  sizeSelected={size => sizeSelected(size)}
                />

                {/* Disabled for now as only want to buy 1 item each time */}
                {/* <Quantity quantity={quantity} quantitySelected={(quantity) => setQuantity(quantity)} sizeQuantity={product.inventory[selectedSize]} />
                 */}
                <button
                  type="submit"
                  className="submit"
                  disabled={!selectedSize}
                  onClick={e => {
                    addToCart(
                      product.model,
                      selectedSize,
                      quantity,
                      product.price.unit_amount,
                    )
                    e.preventDefault()
                  }}
                >
                  Add to cart
                </button>
              </form>
            </div>
          </div>
          {/* <ProductInfo product={product} /> */}
        </div>
      </section>
    </main>
  )
}

export default SingleProduct
