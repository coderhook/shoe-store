import React, {useState} from 'react'
import { IProduct } from '../interfaces'

interface Props {
  product: IProduct
  sizeSelected: (s: string) => void
}

const Sizes: React.FC<Props> = ({product, sizeSelected}) => {
  const [select, setSelect] = useState<string | null>(null)
  return (
    <div
      style={{
        display: 'flex',
        marginBottom: '1.5rem',
        flexWrap: 'wrap',
        textAlign: 'center',
      }}
    >
      {product.inventory && Object.entries(product.inventory).map(
        size =>
          size[1]! > 0 && (
            <div
              // type="button"
              data-testid="size"
              key={size[0]}
              style={{
                flex: '1 0 21%',
                cursor: 'pointer',
                margin: '0.5rem',
                padding: '0.25rem',
                textAlign: 'center',
                background: select === size[0] ? '#9f9fae' : '#e7e7e7',
                color: select === size[0] ? 'white' : '#9f9fae',
              }}
              onClick={() => {
                setSelect((size[0] !== select) ? size[0] : null)
                sizeSelected(size[0])
              }}
            >
              {size[0].substring(1)}
            </div>
          ),
      )}
    </div>
  )
}

export default Sizes
