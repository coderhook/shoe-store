import React, {Component} from 'react'
import {connect} from 'react-redux'
import HeaderNav from '../global/HeaderNav'

class ProductsHeader extends Component {
  render() {
    return (
      <header className="push" role="products-header">
        <HeaderNav />
      </header>
    )
  }
}

const mapStateToProps = state => state

export default connect(mapStateToProps)(ProductsHeader)
