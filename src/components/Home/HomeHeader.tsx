import React from 'react'
import {Link} from 'react-router-dom'
import HeaderNav from '../global/HeaderNav'
import * as Header from '../../assets/img/headers/nature-shoes.jpg'

const HeaderStyle = {
  backgroundImage: `url(${Header})`,
  backgroundRepeat: 'no-repeat',
  backgroundAttachment: 'scroll',
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundOrigin: 'border-box',
}

const layerStyle: React.CSSProperties = {
  backgroundColor: 'rgba(0, 0, 0, 0.25)',
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
}

const HomeHeader: React.FC = () => (
  <header className="large-header light push" style={HeaderStyle}>
    <HeaderNav />

    <div className="header-container" style={layerStyle} role="home-header">
      <div className="content">
        <h1>I love Shoes. I love Nature.</h1>
        <Link to="/products" className="btn" style={{marginTop: '250px'}}>
          I love EcoShoes
        </Link>
      </div>
    </div>

    <div className="down-arrow" aria-hidden="true">
      <span className="arrow"> &#10095; </span>
    </div>
  </header>
)

export default HomeHeader
