import React from 'react'
import {useSelector} from 'react-redux'
import { RootReducer } from '../../ducks'
import { FixMeLater } from '../../utils/interfaces'
import ProductImage from '../Products/ProductImage'


const TopPicks: React.FC = () => {

  const { collections, products }: Pick<RootReducer, 'collections' | 'products'> = useSelector((state: RootReducer) => state)

    let TopPicksToMap: any[] = []

    const collectionsData = collections.collections.data

    const productsData = products.products.data


    let TopPicks

    try {
      TopPicks = collectionsData.find((collection: { slug: string }) => {
        return collection.slug === 'top_picks'
      })
    } catch (e) {
      TopPicks = collectionsData[0]
    }

    let TopPicksProductIDs

    try {
      TopPicksProductIDs = TopPicks.relationships.products.data

      TopPicksProductIDs.forEach(function (productref: {id: string}) {
        var TopPicksProduct = productsData.find(function (product: { id: string }) {
          return product.id === productref.id
        })
        TopPicksToMap.push(TopPicksProduct)
      })


      return (
        <div>
          {TopPicksToMap.map(function (top_pick: FixMeLater) {
            let background

            if (top_pick.background_colour) {
              background = top_pick.background_colour
            } else {
              background = '#d9d9d9'
            }

            var isNew = null

            if (top_pick.new === true) {
              isNew = 'new'
            }

            return (
              <a
                className={`product-item ${isNew}`}
                href={'/product/' + top_pick.id}
                key={top_pick.id}
                id={top_pick.id}
              >
                <div className="product-image" style={{background: background}}>
                  <ProductImage product={top_pick} />
                </div>
                <div className="overlay">
                  <div
                    className="overlay-background"
                    style={{background: '#ad9d8b'}}
                  />
                  <div className="overlay-content">
                    <div className="title">{top_pick.name}</div>
                    <div className="price">
                      {'$' + top_pick.meta.display_price.with_tax.amount / 100}
                    </div>
                  </div>
                </div>
              </a>
            )
          })}
        </div>
      )
    } catch (err) {
      TopPicksProductIDs = null
      return (
        <div className="content">
          <p>You do not have any products attached to a collection.</p>
        </div>
      )
    }
}

export default TopPicks
