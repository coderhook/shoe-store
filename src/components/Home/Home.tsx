import React, {useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'

import HomeHeader from './HomeHeader'
import HomeMainSection from './HomeMainSection'
import MobileNav from '../global/Mobile/MobileNav'
import Loading from '../global/Loading'

import {GetProducts} from '../../ducks/products'
import {GetCategories} from '../../ducks/categories'
import {GetCollections} from '../../ducks/collections'
import {RootReducer} from '../../ducks'

const Home: React.FC = () => {
  const {products, categories, collections} = useSelector(
    (
      state: RootReducer,
    ): Pick<RootReducer, 'products' | 'categories' | 'collections'> => state,
  )
  const dispatch = useDispatch()

  useEffect(() => {
    if (!products.fetched) {
      dispatch(GetProducts())
    }

    if (!categories.fetched) {
      dispatch(GetCategories())
    }

    if (!collections.fetched) {
      dispatch(GetCollections())
    }
  }, [])

  const loading =
    !collections.collections || !products.products || !categories.categories

  return (
    <div>
      <MobileNav />
      <HomeHeader />
      {loading ? <Loading /> : <HomeMainSection />}
    </div>
  )
}

export default Home
