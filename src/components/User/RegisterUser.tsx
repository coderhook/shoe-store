import React, {useState} from 'react'
import {useForm} from 'react-hook-form'
import {yupResolver} from '@hookform/resolvers'
import * as yup from 'yup'
import {useDispatch} from 'react-redux'
import {registerUser} from '../../ducks/user'
import {useRouter} from '../../hooks/useRouter'
import {confirmSignUp, signUp} from './amplifyAPI'
import {VerifyAccount} from './VerifyAccount'

const RegisterSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().email().required(),
  password: yup
    .string()
    .min(6, 'Password must be at least 6 characters')
    .required(),
})

type RegisterFormValues = {
  name: string
  email: string
  password: string
}

const RegisterUser: React.FC = () => {
  const [message, setMessage] = useState<string>('')
  const dispatch = useDispatch()
  const {register, handleSubmit, errors} = useForm({
    resolver: yupResolver(RegisterSchema),
  })
  const router = useRouter()
  const [user, setUser] = useState<any>()

  const onSubmit = async (values: RegisterFormValues) => {
    const {body} = await registerUser(values, dispatch, router)
    // setMessage(user.message)
    console.log({body})
    setUser(body)
  }

  // const onSubmit = async ({email, password, name}: RegisterFormValues) => {
  //   const result = await signUp(email, password, name)
  //   console.log('Registering user', {result})
  // }

  if (user && !user.userConfirmed) {
    console.log(user)

    return <VerifyAccount user={user.user.username} />
  }

  return (
    <div style={{maxWidth: '500px', margin: '0 auto'}} role="register-form">
      <form data-testid="register-form" onSubmit={handleSubmit(onSubmit)}>
        <fieldset className="details">
          <div className="form-header">
            <h5>Register Account</h5>
          </div>
          <div className="form-content">
            <div className="form-fields">
              <label className="input-wrap name required">
                <span className="hide-content">Name</span>
                <input
                  placeholder="Name"
                  name="name"
                  type="text"
                  ref={register}
                />
                {errors.name && <p className="error">{errors.name.message}</p>}
              </label>
              <label className="input-wrap email required">
                <span className="hide-content">Email</span>
                <input
                  className="email"
                  placeholder="Email address"
                  name="email"
                  type="email"
                  aria-label="Email"
                  data-testid="email"
                  ref={register}
                />
                {errors.email && (
                  <p className="error">{errors.password.message}</p>
                )}
              </label>
              <label className="input-wrap password required">
                <span className="hide-content">Password</span>
                <input
                  className="password"
                  placeholder="Password"
                  name="password"
                  type="password"
                  aria-label="Password"
                  data-testid="password"
                  ref={register}
                />
                {errors.password && (
                  <p className="error">{errors.password.message}</p>
                )}
              </label>
            </div>
            <button type="submit" data-testid="submit-register">
              Register
            </button>

            {message && <p role="alert-register">{message}</p>}
          </div>
        </fieldset>
      </form>
    </div>
  )
}

export default RegisterUser
