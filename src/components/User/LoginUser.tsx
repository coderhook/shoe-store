import React, {useState} from 'react'
import {useForm} from 'react-hook-form'
import {yupResolver} from '@hookform/resolvers'
import * as yup from 'yup'
import {useDispatch} from 'react-redux'
import {loginUser} from '../../ducks/user'
import {useRouter} from '../../hooks/useRouter'
import {signIn} from './amplifyAPI'
import {VerifyAccount} from './VerifyAccount'

const LoginSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup
    .string()
    .min(6, 'Password must be at least 6 characters')
    .required(),
})

const LoginUser: React.FC = () => {
  const [message, setMessage] = useState('')
  const {register, handleSubmit, errors} = useForm({
    resolver: yupResolver(LoginSchema),
  })
  const dispatch = useDispatch()
  const router = useRouter()
  const [status, setStatus] = useState<string>()

  const handleSignIn = async (values: {email: string; password: string}) => {
    const result = await loginUser(values, dispatch, router)
    setMessage(result.message)
    setStatus(result.status)
  }

  // const handleSignIn = async (values: {email: string; password: string}) => {
  //   const result = await signIn(values.email, values.password)
  //   console.log('Authenticating user', {result})
  // }

  if (status == 'UserNotConfirmedException')
    return <VerifyAccount user={'coderhookdev@gmail.com'} />
  return (
    <div style={{maxWidth: '500px', margin: '0 auto'}} role="login-form">
      <form
        className="login-form"
        data-testid="login-form"
        onSubmit={handleSubmit(handleSignIn)}
      >
        <fieldset className="details">
          <div className="form-header">
            <h5>Login Account</h5>
          </div>
          <div className="form-content">
            <div className="form-fields">
              <label className="input-wrap email required">
                <span className="hide-content">Email address</span>
                <input
                  placeholder="Email address"
                  name="email"
                  type="text"
                  data-testid="email"
                  ref={register}
                />
              </label>
              {errors.email && <p className="error">{errors.email.message}</p>}

              <label className="input-wrap password required">
                <span className="hide-content">Password</span>
                <input
                  placeholder="Password"
                  name="password"
                  type="password"
                  data-testid="password"
                  ref={register}
                />
              </label>
              {errors.password && (
                <p className="error">{errors.password.message}</p>
              )}
            </div>

            <button type="submit" data-testid="submit-login">
              Login
            </button>
            {message && <p role="alert-login">{message}</p>}
          </div>
        </fieldset>
      </form>
    </div>
  )
}

export default LoginUser
