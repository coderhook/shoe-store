import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import {render} from '../../../utils/test-utils'
import SigninContainer from '../SigninContainer'
import userEvent from '@testing-library/user-event'
import {cleanup, waitFor} from '@testing-library/react'
import 'mutationobserver-shim' // This is needed for react-hook-form useForm
import {
  GetUserLogin as GetUserLoginMock,
  GetUserRegistration as GetUserRegistrationMock,
} from '../../../api'
import {build, fake, sequence} from 'test-data-bot'

jest.mock('../../../api')

beforeEach(() => {
  cleanup()
  jest.clearAllMocks()
})

const userBuilder = build('User').fields({
  id: sequence(s => s),
  name: fake(f => f.name.findName()),
  email: sequence(x => `user-${x}@test.com`),
  password: sequence(p => `Password.${p}`),
})

const user = userBuilder()

const data = {
  token: 'testToken',
  user: {
    email: user.email,
    name: user.name,
  },
}

const TEST_ERROR = 'test error'

describe('Signin Container', () => {
  test('should load login component', () => {
    const res = render(<SigninContainer />)

    expect(res.getByText(/Sign in/i)).toBeInTheDocument()
    expect(res.getByTestId('login-form')).toBeInTheDocument()
  })

  test('should be able to switch to register component from login', () => {
    const res = render(<SigninContainer />)

    expect(res.getByTestId('login-form')).toBeInTheDocument()

    const selectRegister = res.getByText('Register')
    userEvent.click(selectRegister)

    expect(res.getByTestId('register-form')).toBeInTheDocument()
    expect(selectRegister).toHaveTextContent(/register/i)
  })

  describe('Login', () => {
    test('should show user name  after successful login', async () => {
      GetUserLoginMock.mockResolvedValueOnce(
        Promise.resolve({json: () => Promise.resolve({data})}),
      )
      const res = render(<SigninContainer />)

      userEvent.type(res.getByTestId('email'), user.email)
      userEvent.type(res.getByTestId('password'), user.password)
      userEvent.click(res.getByTestId('submit-login'))

      await waitFor(() => expect(res.getByText(user.name)).toBeInTheDocument())
    })

    test('should get error when login does not work', async () => {
      const errorData = {message: 'request failed!'}
      GetUserLoginMock.mockResolvedValueOnce(
        Promise.resolve({json: () => Promise.resolve({data: errorData})}),
      )

      const res = render(<SigninContainer />)

      userEvent.type(res.getByTestId('email'), user.email)
      userEvent.type(res.getByTestId('password'), user.password)
      userEvent.click(res.getByTestId('submit-login'))

      await waitFor(() => {
        expect(GetUserLoginMock).toHaveBeenCalledWith({
          email: user.email,
          password: user.password,
        })
        expect(res.queryByText(user.name)).toBeFalsy()
      })
    })

    test('should render an error message from the server on failed login', async () => {
      GetUserLoginMock.mockRejectedValueOnce({message: TEST_ERROR})

      const res = render(<SigninContainer />)

      userEvent.type(res.getByTestId('email'), user.email)
      userEvent.type(res.getByTestId('password'), user.password)
      userEvent.click(res.getByTestId('submit-login'))

      await waitFor(() =>
        expect(res.getByRole('alert-login')).toHaveTextContent(TEST_ERROR),
      )
    })
  })

  describe('Register', () => {
    test('should register a user and after it show his data on the header bar', async () => {
      GetUserRegistrationMock.mockResolvedValueOnce(
        Promise.resolve({json: () => Promise.resolve({data})}),
      )

      const res = render(<SigninContainer />)

      const selectRegister = res.getByText('Register')
      userEvent.click(selectRegister)
      const nameField = res.getByLabelText(/name/i)
      const emailField = res.getByLabelText(/email/i)
      const passwordField = res.getByLabelText(/password/i)
      const registerButton = res.getByTestId('submit-register')

      userEvent.type(nameField, user.name)
      userEvent.type(emailField, user.email)
      userEvent.type(passwordField, user.password)
      userEvent.click(registerButton)

      expect(res.getByTestId('register-form')).toBeInTheDocument()
      await waitFor(() => {
        expect(res.getByText(user.name)).toBeInTheDocument()
      })
    })

    test('should render an error message from the server on failed register user', async () => {
      GetUserRegistrationMock.mockRejectedValueOnce({message: TEST_ERROR})

      const res = render(<SigninContainer />)

      const selectRegister = res.getByText('Register')
      userEvent.click(selectRegister)
      const nameField = res.getByLabelText(/name/i)
      const emailField = res.getByLabelText(/email/i)
      const passwordField = res.getByLabelText(/password/i)
      const registerButton = res.getByTestId('submit-register')

      userEvent.type(nameField, user.name)
      userEvent.type(emailField, user.email)
      userEvent.type(passwordField, user.password)
      userEvent.click(registerButton)

      await waitFor(() =>
        expect(res.getByRole('alert-register')).toHaveTextContent(TEST_ERROR),
      )
    })
  })
})
