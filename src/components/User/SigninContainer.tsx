import React, {useState} from 'react'
import LoginUser from './LoginUser'
import HeaderNav from '../global/HeaderNav'

import RegisterUser from './RegisterUser'

const SigninContainer: React.FC = () => {
  const [register, setRegister] = useState<boolean>(false)
  return (
    <>
      <HeaderNav />

      <div style={{margin: '8rem 0 3rem 0', textAlign: 'center'}}>
        {!register ? <LoginUser /> : <RegisterUser />}
        <span>
          You can{' '}
          <a style={a} onClick={() => setRegister(false)}>
            <u>Login</u>
          </a>{' '}
          or{' '}
          <a style={a} onClick={() => setRegister(true)}>
            <u>Register</u>
          </a>{' '}
          your account .
        </span>
      </div>
    </>
  )
}

const a = {cursor: 'pointer'}

export default SigninContainer
