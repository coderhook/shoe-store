import {Auth} from 'aws-amplify'
import {sign} from 'crypto'
import user from '../../ducks/user'
import {FixMeLater} from '../../utils/interfaces'

type SignUp = (email: string, password: string, name: string) => void

export const signUp: SignUp = async (email, password, name) => {
  try {
    const user = await Auth.signUp({
      username: email,
      password,
      attributes: {
        name,
        email,
      },
    })
    console.log({user})

    return user
  } catch (error) {
    console.log('error signing up:', error)
  }
}

export const signIn = async (email: string, password: string) => {
  try {
    const user: FixMeLater = await Auth.signIn(email, password)
    console.log(user)

    const {name, email_verified} = user.attributes
    return {
      data: {
        user: {
          name,
          email,
          email_verified,
        },
        token: user.signInUserSession.accessToken.jwtToken,
      },
      message: 'success',
    }
  } catch (error) {
    console.log('Error signing in', error)
    return {
      message: error.message,
      status: error.code,
    }
  }
}

export const confirmSignUp = async (user: FixMeLater, code: string) => {
  try {
    const confirmation = await Auth.confirmSignUp(user, code)
    console.log({confirmation})
  } catch (error) {
    console.log('error confirming sign up', error)
  }
}
