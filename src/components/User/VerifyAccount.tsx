import React, {useState} from 'react'
import {confirmSignUp} from './amplifyAPI'

export const VerifyAccount: React.FC<{user: string}> = user => {
  const [code, setCode] = useState('')
  console.log('verifyAccount', {user})
  const onSubmit = async () => {
    console.log({code, user})
    const result = await confirmSignUp(user.user, code)
    console.log('onSubmit', {result})
  }
  return (
    <>
      <div style={{maxWidth: '500px', margin: '0 auto'}} role="register-form">
        <div className="form-header">
          <h5>Validate Account</h5>
        </div>
        <div className="form-content">
          <p>
            We have send you a verification code to your email, to verify your
            account introduce it here.
          </p>
          <div className="form-fields">
            <label className="input-wrap name required">
              <span className="hide-content">Validation Code</span>
              <input
                placeholder="Validation Code"
                name="code"
                type="text"
                value={code}
                onChange={event => setCode(event.target.value)}
              />
              {/* {errors.name && <p className="error">{errors.name.message}</p>} */}
            </label>
            <button
              type="submit"
              onClick={onSubmit}
              data-testid="submit-validation"
            >
              Verify
            </button>
          </div>
        </div>
      </div>
    </>
  )
}
