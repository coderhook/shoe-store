import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import {render} from '../../../utils/test-utils'
import {cleanup, fireEvent} from '@testing-library/react'
import {useSelector} from 'react-redux'
import productsMock from '../../../utils/sampleData/products'
import categoriesMock from '../../../utils/sampleData/categoriesMock'
import Categories from '../Categories'

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}))

afterEach(cleanup)

describe('CATEGORIES', () => {
  test('should render Categories component', () => {
    useSelector.mockReturnValue({products: [], categories: []})
    const res = render(<Categories />)
    expect(res.container.textContent).toBe('You have no categories')
  })

  test('should render categories with no products when there are no products attached', () => {
    useSelector.mockReturnValue({
      products: productsMock.data,
      categories: [
        {id: 1, name: 'category 1', relationships: {products: null}},
      ],
    })
    const res = render(<Categories />)

    expect(res.container.textContent).toBe(
      'No products related to your categories',
    )
  })

  test('should render categories with related Products when there are products attached', () => {
    useSelector.mockReturnValue({
      products: productsMock.data,
      categories: categoriesMock.data,
    })
    const res = render(<Categories />)

    expect(res.getAllByText(/category 1/i)).toHaveLength(1)
  })
})
