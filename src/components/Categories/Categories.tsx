import React from 'react'
import {useSelector} from 'react-redux'
import ProductImage from '../Products/ProductImage'
import {useDispatch} from 'react-redux'
import {useHistory} from 'react-router-dom'

import {SET_STYLE} from '../../ducks/styles'
import { RootReducer } from '../../ducks'
import { Category, IProduct } from '../interfaces'

const defaultBackgroundProduct = '#d9d9d9'

const Categories: React.FC = () => {
  const {products, categories}: {products: IProduct[], categories: Category[]} = useSelector((state: RootReducer) => ({
    products: state.products.products.data,
    categories: state.categories.categories.data,
  }))

  const history = useHistory()
  const dispatch = useDispatch()

  const ChangeStyle = (name: string) => {
    dispatch({type: SET_STYLE, style: name})
    history.push('/styles')
  }

  if (categories.length > 0) {
    return (
      <div className="styles-list">
        {categories.map((category, i) => {
          if (!!category.relationships.products) {
            const CatProductRef = category.relationships.products.data[0]
            const CatProduct = products.find(function (product) {
              return product.model === CatProductRef.model
            })

            const background = CatProduct?.background_colour
              ? CatProduct.background_colour
              : defaultBackgroundProduct

            return (
              <a
                data-testid="category-link"
                key={`${category.name}`}
                className="styles-item"
                href="styles"
                style={{background}}
                // name={category.name}
                onClick={e => {
                  e.preventDefault()
                  ChangeStyle(category.name)
                }}
              >
                <h3>{category.name}</h3>
                <ProductImage
                  product={CatProduct}
                  aria-hidden="true"
                />
                <div
                  className="overlay fake-btn"
                  style={{background: '#4d4d4d'}}
                >
                  Shop now
                </div>
              </a>
            )
          } else {
            return (
              <div className="content" key={i}>
                <p>No products related to your categories</p>
              </div>
            )
          }
        })}
      </div>
    )
  } else {
    return (
      <div className="styles-list">
        <div className="content">
          <p>You have no categories</p>
        </div>
      </div>
    )
  }
}

export default Categories
