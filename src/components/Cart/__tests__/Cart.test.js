import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import {render} from '../../../utils/test-utils'
import productsMock from '../../../utils/sampleData/products.json'
import cartMock from '../../../utils/sampleData/cart.json'
import Cart from '../Cart'
import userEvent from '@testing-library/user-event'

const getCartInitialState = cartInitialState => ({
  cart: cartInitialState,
  products: {products: productsMock},
})

describe('Cart', () => {
  test('should not show items when cart is empty', () => {
    const initialState = getCartInitialState({cart: []})

    const res = render(<Cart />, {initialState})

    expect(res.getByTestId('quantity')).toHaveTextContent('0')
  })

  test('should show items on the cart', () => {
    const initialState = getCartInitialState(cartMock)

    const res = render(<Cart />, {initialState})

    expect(res.getByText(/product 1/i)).toBeInTheDocument()
    expect(res.getByTestId('quantity')).toHaveTextContent(
      initialState.cart.cart.length,
    )
  })

  test('should be able to increase or decrease product', () => {
    const initialState = getCartInitialState(cartMock)
    const res = render(<Cart />, {initialState})

    userEvent.click(res.getByRole('increment'))
    expect(res.getByRole('display-quantity')).toHaveValue(
      initialState.cart.cart[0].quantity + 1,
    )

    userEvent.click(res.getByRole('decrement'))
    userEvent.click(res.getByRole('decrement'))
    expect(res.getByRole('display-quantity')).toHaveValue(
      initialState.cart.cart[0].quantity - 1,
    )
  })

  test('should be able to delete an item on the cart', () => {
    const initialState = getCartInitialState(cartMock)
    const res = render(<Cart />, {initialState})

    expect(res.getByText(/product 1/i)).toBeInTheDocument()

    userEvent.click(res.getByRole('delete-item'))
    expect(res.queryByText(/product 1/i)).not.toBeInTheDocument()
  })
})
