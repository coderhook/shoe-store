import React from 'react'
import {useSelector} from 'react-redux'
import {Link} from 'react-router-dom'
import {State} from '../../ducks'

import MobileNav from '../global/Mobile/MobileNav'
import {ICartItem} from '../interfaces'
import CartHeader from './CartHeader'
import CartItems from './CartItems'

export const getTotalAmount = (cart: ICartItem[]) =>
  cart.reduce(
    (acc: number, item) => acc + (item.price * item.quantity) / 100,
    0,
  )

const Cart: React.FC = () => {
  const {cart, products} = useSelector((state: State) => ({
    cart: state.cart.cart,
    products: state.products.products,
  }))
  if (!cart) {
    return (
      <div>
        <MobileNav />
        <CartHeader />
        <main role="main" id="container" className="main-container push">
          <section>
            <div className="content">
              <div className="loading">
                <div className="loading-icon" aria-hidden="true">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 106 54">
                    <path
                      fill="currentColor"
                      d="M77.6,18.3c0,3.2-1.2,6.4-3.7,8.8l-21,21l-21-21c-4.9-4.9-4.9-12.8,0-17.7c2.4-2.4,5.6-3.7,8.9-3.7
              c3.2,0,6.4,1.2,8.8,3.7l3.3,3.3l3.3-3.3c4.9-4.9,12.8-4.9,17.7,0C76.3,11.9,77.6,15.1,77.6,18.3z"
                    />
                  </svg>
                </div>
                <p className="loading-text">Loading</p>
              </div>
            </div>
          </section>
        </main>
      </div>
    )
  }

  if (cart[0]) {
    const subtotal = getTotalAmount(cart).toFixed(2) + ' €'
    return (
      <div>
        <MobileNav />
        <CartHeader />
        <main role="main" id="container" className="main-container push">
          <section className="cart">
            <div className="content">
              <form className="cart-listing" method="post" noValidate>
                <div className="cart-list-headings">
                  <div className="cart-product-header">Product</div>
                  <div className="cart-header-group">
                    <div className="cart-empty-header" />
                    <div className="cart-quantity-header">Quantity</div>
                    <div className="cart-price-header">Price</div>
                    <div className="cart-price-header">Size</div>
                  </div>
                </div>
                <CartItems items={cart} products={products.data} />
                <div className="total-price">
                  Subtotal<span className="hide-content"> of all products</span>{' '}
                  <span className="price">{subtotal}</span>
                </div>
                <Link className="btn submit" to="/checkout">
                  Checkout
                </Link>
              </form>
            </div>
          </section>
        </main>
      </div>
    )
  } else {
    return (
      <div>
        <MobileNav />
        <CartHeader />
        <main role="main" id="container" className="main-container push">
          <section className="cart">
            <div className="content">
              <div className="cart-listing empty">
                <p>
                  Oh no, looks like you don't love natural shoes, as your cart
                  is empty.
                </p>
                <Link className="btn" to="/products">
                  Start shopping
                </Link>
              </div>
            </div>
          </section>
        </main>
      </div>
    )
  }
}

export default Cart
