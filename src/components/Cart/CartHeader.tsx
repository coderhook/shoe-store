import React from 'react'
import {withRouter} from 'react-router-dom'
import { FixMeLater } from '../../utils/interfaces'

import HeaderNav from '../global/HeaderNav'

const CartHeader: React.FC<FixMeLater> = props => {
  let headerText

  if (props.location.pathname.includes('cart')) {
    headerText = 'Shopping Cart'
  } else if (props.location.pathname.includes('checkout')) {
    headerText = 'Checkout'
  } else if (props.location.pathname.includes('order-confirmation')) {
    headerText = 'Order Confirmation'
  }

  return (
    <header className="push">
      <HeaderNav />
      <div className="header-container smaller">
        <div className="content">
          <h1>{headerText}</h1>
        </div>
      </div>
    </header>
  )
}

export default withRouter(CartHeader)
