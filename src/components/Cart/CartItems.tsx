import React from 'react'
import {useDispatch} from 'react-redux'
import ProductImage from '../Products/ProductImage'
import {
  FETCH_CART_START,
  FETCH_CART_END,
  DELETE_ITEM,
  UPDATE_ITEM,
} from '../../ducks/cart'
import {ICartItem, IProduct} from '../interfaces'

type Props = {
  items: ICartItem[]
  products: IProduct[]
}
type CartMethod = (a: number, b: ICartItem) => void

const CartItems: React.FC<Props> = ({items, products}) => {
  const dispatch = useDispatch()

  const cart_decrement: CartMethod = (itemIndex, item) => {
    const updatedCart = [
      ...items.slice(0, itemIndex),
      {...item, quantity: item.quantity - 1},
      ...items.slice(itemIndex + 1),
    ]
    dispatch({type: UPDATE_ITEM, payload: updatedCart})
  }

  const cart_increment: CartMethod = (itemIndex, item) => {
    const updatedCart = [
      ...items.slice(0, itemIndex),
      {...item, quantity: item.quantity + 1},
      ...items.slice(itemIndex + 1),
    ]
    dispatch({type: UPDATE_ITEM, payload: updatedCart})
  }

  const delete_item = (itemIndex: number) => {
    const cartUpdated = [
      ...items.slice(0, itemIndex),
      ...items.slice(itemIndex + 1),
    ]
    dispatch({type: DELETE_ITEM, payload: cartUpdated})
  }

  return (
    <div>
      {items.map((item, i) => {
        const product = products.find(_product => {
          return _product.model === item.model
        })

        const background = product?.background_colour || 'white'

        const TotalPriceHidden = item?.quantity > 1 ? '' : 'hidden'

        if (!product) return
        const totalPriceItem = (product.price.unit_amount / 100) * item.quantity

        return (
          <div className="cart-item" key={item.id} data-testid="cart-item">
            <div className="product-image" aria-hidden="true">
              <ProductImage product={product!} />
            </div>
            <div className="cart-details">
              <div className="cart-title">
                <h3>{product.name}</h3>
              </div>

              <div className="cart-quantity">
                <div className="quantity-input">
                  <p className="hide-content">Product quantity.</p>
                  <p className="hide-content">
                    Change the quantity by using the buttons, or alter the input
                    directly.
                  </p>
                  <button
                    type="button"
                    className="decrement number-button"
                    role="decrement"
                    disabled={item.quantity <= 0}
                    onClick={() => {
                      cart_decrement(i, item)
                    }}
                  >
                    <span className="hide-content">Decrement quantity</span>
                    <span aria-hidden="true">-</span>
                  </button>
                  <input
                    className="quantity"
                    role="display-quantity"
                    name="number"
                    type="number"
                    min="1"
                    max="10"
                    size={2}
                    // defaultValue={item.quantity}
                    value={item.quantity}
                    disabled
                  />
                  <button
                    type="button"
                    className="increment number-button"
                    role="increment"
                    onClick={() => {
                      cart_increment(i, item)
                    }}
                  >
                    <span className="hide-content">Increment quantity</span>
                    <span aria-hidden="true">+</span>
                  </button>
                </div>
              </div>
              <div className="cart-price">
                <p className="price">
                  <span className={`item-price ${TotalPriceHidden}`}>
                    <span className="hide-content">Price per item </span>$
                    <span className="product-price">
                      {product.price.unit_amount / 100}
                    </span>
                    <span aria-hidden="true"> / </span>
                  </span>
                  <span className="hide-content">Product subtotal </span>$
                  <span className="total-product-price">
                    {totalPriceItem.toFixed(2)}
                  </span>
                </p>
              </div>
              <div className="cart-price">
                <p className="price">{item.size.substring(1)}</p>
              </div>
            </div>
            <div className="cart-delete">
              <button
                className="remove"
                role="delete-item"
                type="button"
                onClick={() => {
                  delete_item(i)
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 15.55635 15.55635"
                >
                  <rect
                    fill="currentColor"
                    x="-2.22183"
                    y="6.77817"
                    width="20"
                    height="2"
                    transform="translate(7.77817 -3.22183) rotate(45)"
                  />
                  <rect
                    fill="currentColor"
                    x="-2.22183"
                    y="6.77817"
                    width="20"
                    height="2"
                    transform="translate(18.77817 7.77817) rotate(135)"
                  />
                </svg>
              </button>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default CartItems
