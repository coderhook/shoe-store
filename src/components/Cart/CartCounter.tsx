import React from 'react'
import {Link} from 'react-router-dom'

type Props = {
  quantity: number
}

const CartCounter: React.FC<Props> = ({quantity}) => {
  return (
    <Link
      to="/cart"
      className="cart"
      aria-live="polite"
      data-testid="cart-link"
    >
      <span className="cart-name" aria-hidden="true">
        Cart (
      </span>
      <span className="hide-content">The cart contains </span>
      <span className="cart-count" data-testid="quantity">
        {quantity}
      </span>
      <span className="hide-content">items.</span>
      <span className="cart-name" aria-hidden="true">
        )
      </span>
    </Link>
  )
}

export default CartCounter
