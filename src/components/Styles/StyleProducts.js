import React from 'react'
import {connect} from 'react-redux'
import ProductImage from '../Products/ProductImage'
import {useHistory} from 'react-router-dom'

function mapStateToProps(state) {
  return state
}

const StyleProducts = props => {
  console.log('HERE', {props})
  var productsToMap = []
  var categories = props.categories.categories.data
  var CurrentStyle = props.styles.style
  var CurrentCategory = categories.find(category => {
    return category.name === CurrentStyle
  })

  const history = useHistory()

  try {
    var CurrentCategoryProductIDs = CurrentCategory.relationships.products.data
    var Products = props.products.products
    var ProductsData = props.products.products.data

    CurrentCategoryProductIDs.forEach(function (productref) {
      var Product = ProductsData.find(function (product) {
        return product.model === productref.model
      })
      productsToMap.push(Product)
    })

    return (
      <div className="product-list">
        {productsToMap.map(function (product) {
          let background

          if (product.background_colour) {
            background = product.background_colour
          } else {
            background = '#d9d9d9'
          }

          return (
            <a
              className="product-item"
              href={'product/' + product.model}
              key={product._id}
              onClick={e => {
                e.preventDefault()
                history.push('product/' + product.model)
              }}
            >
              <div className="product-image" style={{background: background}}>
                <ProductImage product={product} products={Products} />
              </div>
              <div className="overlay">
                <div
                  className="overlay-background"
                  style={{background: '#aaaaaa'}}
                />
                <div className="overlay-content">
                  <div className="title">{product.name}</div>
                  <div className="price">
                    {product.price.unit_amount / 100 + '€'}
                  </div>
                </div>
              </div>
            </a>
          )
        })}
      </div>
    )
  } catch (err) {
    return (
      <div className="content">
        <h2>Your category has no attached products</h2>
      </div>
    )
  }
}

export default connect(mapStateToProps)(StyleProducts)
