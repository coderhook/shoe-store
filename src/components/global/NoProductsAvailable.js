import React from 'react'

const NoProductsAvailable = () => {
  return (
    <main role="main" id="container" className="main-container push">
      <section className="products">
        <div
          className="content"
          role="no-products"
          style={{textAlign: 'center'}}
        >
          <p>There are no products available</p>
        </div>
      </section>
    </main>
  )
}

export default NoProductsAvailable
