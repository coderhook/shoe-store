import React, {useEffect, useState} from 'react'
import {useSelector} from 'react-redux'
import {Link} from 'react-router-dom'
import LogoLoveShoes from './LogoLoveShoes'

import CartCounter from '../Cart/CartCounter'

const HeaderNav = () => {
  const cart = useSelector(state => state.cart.cart)
  const user = useSelector(state => state.user.user)
  const [quantity, setQuantity] = useState(cart.length)

  useEffect(() => {
    setQuantity(cart.length)
  }, [cart.length])

  return (
    <div className="nav-container">
      <nav className="primary-nav light">
        <Link to="/products" role="products-link">
          Products
        </Link>

        <Link to="/styles">Styles</Link>
      </nav>
      <div className="logo light">
        <Link to="/" className="logo-link" role="logo-link">
          <span className="hide-content">I love Shoes</span>
          <LogoLoveShoes />
        </Link>
      </div>

      <nav className="secondary-nav light">
        {user ? (
          <>
            Hello, <Link to="">{user.name}</Link>
          </>
        ) : (
          <Link to="/signin">Sign in</Link>
        )}
        <CartCounter quantity={quantity} />
      </nav>
    </div>
  )
}

export default HeaderNav
