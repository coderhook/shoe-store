import React from 'react'
import MobileNav from '../global/Mobile/MobileNav'
import Footer from './Footer'
import HeaderNav from './HeaderNav'

const style = {
  body: {
    textAlign: 'center',
    padding: '40px 0',
    background: '#EBF0F5',
  },
  h1: {
    color: '#88B04B',
    fontWeight: '900',
    fontSize: '40px',
    marginBottom: '10px',
  },
  p: {
    color: '#404F5E',
    fontSize: '20px',
    margin: 'auto',
  },
  i: {
    color: '#9ABC66',
    fontSize: '100px',
    lineHeight: '200px',
    marginLeft: '-15px',
  },
  card: {
    background: 'white',
    padding: '60px',
    borderRadius: '4px',
    boxShadow: '0 2px 3px #C8D0D8',
    display: 'inlineBlock',
    margin: 'auto',
  },
}

const SuccessPayment = () => (
  <div>
    <MobileNav />
    <HeaderNav />
    <main role="main" id="container" className="main-container push">
      <section style={style.body}>
        <div>
          <div class="card" style={style.card}>
            <div
              style={{
                borderRadius: '200px',
                height: '200px',
                width: '200px',
                background: '#F8FAF5',
                margin: '0 auto',
              }}
            >
              <i class="checkmark" style={style.i}>
                ✓
              </i>
            </div>
            <h1 style={style.h1}>Success</h1>
            <p style={style.p}>
              We received your payment
              <br /> Soon you will receive your shoes!
            </p>
          </div>
        </div>
      </section>
    </main>
    <Footer />
  </div>
)

export default SuccessPayment
