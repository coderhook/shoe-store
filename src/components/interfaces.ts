import { State } from "../ducks";
import { Optional } from "../utils/interfaces";

export type StateProducts = Pick<State, 'products'>

// Products
export interface IProduct {
    model: string
    name: string
     description: string
     price : {
       unit_amount: number,
       currency:  'eur' 
    }
     images: string[],
     category: string
     inventory : Sizes
     background_colour: Optional<string>
  }


type Sizes = {
    p32?: number
    p33?: number
    p34?: number
    p35?: number
    p36?: number
    p37?: number
    p38?: number
    p39?: number
    p40?: number
    p41?: number
    p42?: number
    p43?: number
    p44?: number
    p45?: number
}

// End Products

//Category
export interface Category {
    id: string
    name: string
    relationships: {
        products: {
            data: IProduct[]
        }
    }
}

// End Category

// Cart
export interface ICartItem {
    id: string
    model: string
    price: number
    quantity: number
    size: keyof Sizes
  }

// End Cart