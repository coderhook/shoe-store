const MoltinGateway = require('@moltin/sdk').gateway

let client_id = 'j6hSilXRQfxKohTndUuVrErLcSJWP15P347L6Im0M4'

if (process.env.REACT_APP_MOLTIN_CLIENT_ID) {
  client_id = process.env.REACT_APP_MOLTIN_CLIENT_ID
}

const Moltin = MoltinGateway({
  client_id,
  application: 'react-demo-store',
})

// export const GetProducts = async () => {
//   const getProductsLog = await Moltin.Products.With('files, main_images, collections').All();
//   console.log('HERE!!!!!', {getProductsLog})
//   return getProductsLog
// }

const productsSample = {
  data: [
    {
      model: '1',
      name: 'Product 1',
      description: 'Descriptio Product 1',
      price: {
        unit_amount: 5990,
        currency: 'eur',
      },
      images: ['https://picsum.photos/50'],
      category: 'Ibicenca',
      inventory: {
        p35: 1,
        p36: 0,
        p37: 4,
      },
      background_colour: null,
    },
    {
      model: '2',
      name: 'Product 2',
      description: 'Descriptio Product 2',
      price: {
        unit_amount: 3990,
        currency: 'eur',
      },
      images: ['https://picsum.photos/50'],
      category: 'Ibicenca',
      inventory: {
        p35: 1,
        p36: 2,
        p37: 0,
      },
      background_colour: null,
    },
    {
      model: '3',
      name: 'Product 3',
      description: 'Descriptio Product 3',
      price: {
        unit_amount: 3990,
        currency: 'eur',
      },
      images: ['https://picsum.photos/50'],
      category: 'Ibicenca',
      inventory: {
        p35: 1,
        p36: 2,
        p37: 0,
      },
      background_colour: null,
    },
    {
      model: '4',
      name: 'Product 4',
      description: 'Descriptio Product 4',
      price: {
        unit_amount: 3990,
        currency: 'eur',
      },
      images: ['https://picsum.photos/50'],
      category: 'Ibicenca',
      inventory: {
        p35: 1,
        p36: 2,
        p37: 0,
      },
      background_colour: null,
    },
  ],
}

export const GetProducts = async () => {
  // const response = await fetch('http://localhost:3001/api/v1/products')
  // const {data} = await response.json()
  // console.log('GEtProducts', {data})
  // return {data}
  return {data: productsSample.data}
}

export const GetProduct = async ID => {
  const getProductLog = await Moltin.Products.Get(ID)
  console.log({getProductLog})
  return getProductLog
}

export const GetCategories = async () => {
  // const getCategorieSSSLog = await Moltin.Categories.With('products').All();
  // console.log({ getCategorieSSSLog });

  const categories = {
    data: [
      {
        id: '1',
        name: 'Category 1',
        description: 'category number one',
        relationships: {
          products: {
            data: [
              {
                model: '1',
              },
            ],
          },
        },
      },
      {
        id: '2',
        name: 'Category 2',
        description: 'category number TWO',
        relationships: {
          products: {
            data: [
              {
                model: '2',
              },
            ],
          },
        },
      },
      {
        id: '3',
        name: 'Category 3',
        description: 'category number Three',
        relationships: {
          products: {
            data: [
              {
                model: '3',
              },
            ],
          },
        },
      },
      {
        id: '4',
        name: 'Category 4',
        description: 'category number four',
        relationships: {
          products: {
            data: [
              {
                model: '4',
              },
            ],
          },
        },
      },
    ],
  }

  return categories
}

export const GetCategory = async ID => {
  const getCategoryById = await Moltin.Categories.Get(ID)
  console.log({getCategoryById})
  return getCategoryById
}

export const GetCollections = async () => {
  const getCollectionsLog = await Moltin.Collections.With('products').All()
  console.log({getCollectionsLog})
  return getCollectionsLog
}

export const GetBrands = async () => {
  const getBrandsLog = await Moltin.Brands.All()
  console.log({getBrandsLog})

  return getBrandsLog
}

export const GetFile = ID => {
  const getFileLOG = Moltin.Files.Get(ID)
  console.log({getFileLOG})
  return getFileLOG
}

export const AddCart = async (id, quantity) => {
  const addToCartLOG = await Moltin.Cart().AddProduct(id, quantity)
  console.log({addToCartLOG})
  return addToCartLOG
}

export const UpdateCartPlus = (ID, quantity) =>
  Moltin.Cart().UpdateItemQuantity(ID, quantity + 1)

export const UpdateCartMinus = (ID, quantity) =>
  Moltin.Cart().UpdateItemQuantity(ID, quantity - 1)

export const UpdateCart = (ID, quantity) =>
  Moltin.Cart().UpdateItemQuantity(ID, quantity)

export const GetCartItems = async () => {
  const getCartItems = await Moltin.Cart().Items()
  console.log({getCartItems})
  return getCartItems
}

export const Checkout = async data => {
  const checkOut = await Moltin.Cart().Checkout(data)
  console.log({checkOut})
  return checkOut
}

export const GetOrder = async ID => {
  const getOrder = await Moltin.Orders.Get(ID)
  console.log({getOrder})
  return getOrder
}

export const OrderPay = async (ID, data) => {
  const orderPay = await Moltin.Orders.Payment(ID, data)
  console.log({orderPay})
  return orderPay
}

export const DeleteCart = async () => {
  const deleteCart = await Moltin.Cart().Delete()
  console.log({deleteCart})
  return deleteCart
}
