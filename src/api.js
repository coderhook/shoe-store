
const baseURL = process.env.baseURL || 'http://localhost:3001/api/v1'

// Auth
export const GetUserLogin = async (values) => await fetch(`${baseURL}/auth/login`, {
  method: 'POST',
  body: JSON.stringify(values),
  headers: {
    'Content-Type': 'application/json',
  },
})

export const GetUserRegistration = async (values) => await fetch(`${baseURL}/auth/register`, {
  method: 'POST',
  body: JSON.stringify(values),
  headers: {
    'Content-Type': 'application/json',
  },
})

// Products
export const GetProducts = async () => {
  const response = await fetch(`${baseURL}/products`)
  const {data} = await response.json()
  console.log('GEtProducts', {data})
  return {data}
}


// Categories - [TODO]: implement
export const GetCategories = async () => {
  // const getCategorieSSSLog = await Moltin.Categories.With('products').All();
  // console.log({ getCategorieSSSLog });

  const categories = {
    data: [
      {
        id: '1',
        name: 'Category 1',
        description: 'category number one',
        relationships: {
          products: {
            data: [
              {
                model: '1',
              },
            ],
          },
        },
      },
      {
        id: '2',
        name: 'Category 2',
        description: 'category number TWO',
        relationships: {
          products: {
            data: [
              {
                model: '2',
              },
            ],
          },
        },
      },
      {
        id: '3',
        name: 'Category 3',
        description: 'category number Three',
        relationships: {
          products: {
            data: [
              {
                model: '3',
              },
            ],
          },
        },
      },
      {
        id: '4',
        name: 'Category 4',
        description: 'category number four',
        relationships: {
          products: {
            data: [
              {
                model: '4',
              },
            ],
          },
        },
      },
    ],
  }

  return categories
}