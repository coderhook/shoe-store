// to use it with react script addit to the script test
// currently not using it
module.exports = {
  clearMocks: true,
  testMatch: ['**/__tests__/**/*.test.js?(x)'],
  testEnvironment: 'jest-environment-jsdom-sixteen',

}

// module.export = {
//   roots: ['<rootDir>/src'],
//   // transform: {
//   //   '\\.(js|jsx)?$': 'babel-jest',
//   // },
//   testMatch: ['<rootDir>/src/**/>(*.)test.{js, jsx}'], // finds test
//   testEnviroment: 'jest-environment-jsdom',
//   moduleFileExtensions: ['js', 'jsx', 'json', 'node'],
//   moduleDirectories: [
//     'node_modules',
//     +(
//       // add the directory with the test-utils.js file, for example:
//       (+'src/utils')
//     ), // a utility folder
//     +__dirname, // the root directory
//   ],
//   // moduleDirectories: ['node_modules'],
//   testPathIgnorePatterns: ['/node_modules/', '/public/'],
//   // setupFilesAfterEnv: [
//   //   '@testing-library/jest-dom/extend-expect',
//   //   '@testing-library/react/cleanup-after-each',
//   // ], // setupFiles before the tests are ran
//   // moduleNameMapper: {
//   //   '\\.module\\.css$': 'identity-obj-proxy',
//   //   '\\.css$': require.resolve('./src/utils/style-mock.js'),
//   // },
//   setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect']
// }
